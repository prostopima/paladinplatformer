# README #


Hi. This is a simple platformer where you can run and jump.

It's my first platformer on Unity, so I started from leaning about tools that unity has for such purpose. I decided to use tilemaps, cause it looked very convenient. But I had to research, for some time, how to get rid of graphic artifacts. Then I animated the character, programmed and tunned controls. At the end I worked on graphics. I used post processes and drew some clouds.

I spent about 7-8 hours on research and development.

The project is so simple, that I made only two script files. One to control character and one for character settings.

In project I used unity packages (2D Pixel Perfect, 2D Tilemap Editor, Cinemachine)


If I had more time, I would add sounds, particles and more moving things.
