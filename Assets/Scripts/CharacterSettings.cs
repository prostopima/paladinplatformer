﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CharacterSettings", menuName = "ScriptableObjects/CharacterSettings")]
public class CharacterSettings : ScriptableObject
{
    public float jumpForce;
    public float midairForce;
    public float xVelocity;
    
    
    public float footOffset;
    public float hindFootOffset;
    public float footRayLength;
}
