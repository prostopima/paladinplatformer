﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using TMPro;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    public Rigidbody2D rigidBody;
    public CharacterSettings characterSettings;
    public Animator animator;
    public LayerMask groundLayer;

    
    public bool isOnTheGround;
    protected float orientation;
    
    protected int midAirId;
    protected int runningId;
    protected int jumpId;

    protected bool toJump;


    void Start()
    {
        midAirId = Animator.StringToHash("midAir");
        runningId = Animator.StringToHash("running");
        jumpId = Animator.StringToHash("jump");
    }

    void FixedUpdate()
    {
        isOnTheGround = rigidBody.IsTouchingLayers() && CheckFeet();
        toJump = Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.UpArrow) || toJump;
    }

    private void Update()
    { 
        OnMove();
        OnJump(toJump);
        ClearMovementVariables();

        UpdateCharacterView();
    }

    protected void OnMove()
    {
        int value = Input.GetKey(KeyCode.LeftArrow) ? -1 : (Input.GetKey(KeyCode.RightArrow) ? 1 : 0);
        if(value != 0)
        {
            orientation = value;
            if(isOnTheGround)
            {
                rigidBody.velocity = new Vector2(characterSettings.xVelocity * orientation, rigidBody.velocity.y);
            }
            else
            {
                rigidBody.AddForce(new Vector2(characterSettings.midairForce*orientation, 0), ForceMode2D.Force);
                rigidBody.velocity = new Vector2(Mathf.Clamp(rigidBody.velocity.x,-characterSettings.xVelocity, characterSettings.xVelocity), rigidBody.velocity.y);
            }
            
        }
        else if(isOnTheGround)
        {
            rigidBody.velocity = new Vector2(0, rigidBody.velocity.y);
        }
    }

    protected void OnJump(bool value)
    {
        
        if(value && isOnTheGround)
            rigidBody.AddForce(new Vector2(0f, characterSettings.jumpForce), ForceMode2D.Impulse);
    }

    protected void UpdateCharacterView()
    {
        if(orientation * rigidBody.transform.localScale.x < 0)
        {
            rigidBody.transform.localScale = new Vector3(rigidBody.transform.localScale.x * -1, 1, 1);
        }
        
        if(!animator.GetBool(midAirId) && !isOnTheGround)
            animator.SetTrigger(jumpId);
        animator.SetBool(midAirId, !isOnTheGround);
        animator.SetBool(runningId, isOnTheGround && Mathf.Abs(rigidBody.velocity.x) > 0.1f);
    }

    private void ClearMovementVariables()
    {
        toJump = false;
    }

    protected bool CheckFeet()
    {
        return RaycastFoot(new Vector2(characterSettings.hindFootOffset*orientation*-1, 0f)) ||
               RaycastFoot(new Vector2(characterSettings.footOffset*orientation, 0f));
    }
    
	RaycastHit2D RaycastFoot(Vector2 offset)
	{
		return Raycast(offset, Vector2.down, characterSettings.footRayLength, groundLayer);
	}

	RaycastHit2D Raycast(Vector2 offset, Vector2 rayDirection, float length, LayerMask mask)
	{
		Vector2 pos = rigidBody.transform.position;
    	RaycastHit2D hit = Physics2D.Raycast(pos + offset, rayDirection, length, mask);
		return hit;
	}
}
